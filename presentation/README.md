# squelette de slides beamer

## fonctionnalités

- rappel de section
- exemples de mise en page (tableau, minipage...)
- images
- vidéos (vérifier que les fichiers vidéos sont bien accessibles; ne fonctionne qu'avec certains viewers PDF comme okular)
- algorithmes (package `algorithm2e`)
- code source (package `minted`)
- schémas (package `tikz`)
- bibliographie

## installation de latex

- si possible, installer un `texlive-full` récent
- si packages manquants ou trop anciens, allez voir dans le dossier `old_sty`

## compilation du .tex

```
make
```

[fichier résultat](fichier_resultat.pdf)

