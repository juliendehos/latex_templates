with import <nixpkgs> {}; 

stdenv.mkDerivation {

    name = "env";

    buildInputs = [ 
        ghostscript 
        python3Packages.pygments
        texlive.combined.scheme-full 
        which
    ];

    src = ./.;

    installPhase = ''
        mkdir -p $out
        cp slides.pdf $out/
        mkdir -p $out/images
        cp images/giphy.mp4 $out/images/
    '';

}

